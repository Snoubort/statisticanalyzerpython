import math
import pandas

class TowerAnalyzer:
    controller = None

    metrics_tower: pandas.DataFrame = None
    metrics_zone: pandas.DataFrame = None
    metrics_type: pandas.DataFrame = None
    metrics_comb: pandas.DataFrame = None

    def fill_tower(self, tower_type: str) -> bool:
        if "Tower Type" in self.metrics_tower.columns:
            return False

        unic_combinations = self.controller.stat_table.drop_duplicates(subset=[tower_type])
        self.metrics_tower["Tower Type"] = unic_combinations.loc[:, tower_type]

        self.metrics_tower = self.metrics_tower.reset_index(drop=True)
        return True

    def fill_zone(self, attack_zone: str) -> bool:
        if "Attack Zone" in self.metrics_zone.columns:
            return False

        unic_combinations = self.controller.stat_table.drop_duplicates(subset=[attack_zone])
        self.metrics_zone["Attack Zone"] = unic_combinations.loc[:, attack_zone]

        self.metrics_zone = self.metrics_zone.reset_index(drop=True)
        return True

    def fill_type(self, damage_type: str) -> bool:
        if "Damage Type" in self.metrics_type.columns:
            return False

        unic_combinations = self.controller.stat_table.drop_duplicates(subset=[damage_type])
        self.metrics_type["Damage Type"] = unic_combinations.loc[:, damage_type]

        self.metrics_type = self.metrics_type.reset_index(drop=True)
        return True

    def fill_combinations(self, tower_type: str, attack_zone: str, damage_type: str) -> bool:
        if "Tower Type" in self.metrics_comb.columns:
            return False

        print([tower_type, attack_zone, damage_type])
        unic_combinations = self.controller.stat_table.drop_duplicates(subset=[tower_type, attack_zone, damage_type])
        print(unic_combinations)
        self.metrics_comb["Tower Type"] = unic_combinations.loc[:, tower_type]
        self.metrics_comb["Attack Zone"] = unic_combinations.loc[:, attack_zone]
        self.metrics_comb["Damage Type"] = unic_combinations.loc[:, damage_type]

        self.metrics_comb = self.metrics_comb.reset_index(drop=True)
        return True

    def calc_tower_count(self, stat_header: str, metric_header: str) -> bool:
        if "Tower Count" in self.metrics_tower.columns:
            return False

        for i in range(len(self.metrics_tower)):
            tower_frame = self.controller.stat_table.loc[self.controller.stat_table[stat_header] == self.metrics_tower[metric_header][i]]
            self.metrics_tower.loc[i, "Tower Count"] = len(tower_frame)
        return True

    def calc_zone_count(self, stat_header: str, metric_header: str) -> bool:
        if "Zone Count" in self.metrics_zone.columns:
            return False

        for i in range(len(self.metrics_zone)):
            zone_frame = self.controller.stat_table.loc[self.controller.stat_table[stat_header] == self.metrics_zone[metric_header][i]]
            self.metrics_zone.loc[i, "Zone Count"] = len(zone_frame)
        return True

    def calc_type_count(self, stat_header: str, metric_header: str) -> bool:
        if "Damage Type Count" in self.metrics_type.columns:
            return False

        for i in range(len(self.metrics_type)):
            tower_frame = self.controller.stat_table.loc[self.controller.stat_table[stat_header] == self.metrics_type[metric_header][i]]
            self.metrics_type.loc[i, "Damage Type Count"] = len(tower_frame)
        return True

    def calc_combinations_count(self, stat_header_tower: str, metric_header_tower: str,
                                stat_header_zone: str, metric_header_zone: str,
                                stat_header_type: str, metric_header_type: str) -> bool:
        if "Combination Count" in self.metrics_comb.columns:
            return False

        for i in range(len(self.metrics_comb)):
            comb_frame = self.controller.stat_table.loc[(self.controller.stat_table[stat_header_tower] == self.metrics_comb[metric_header_tower][i]) &
                                              (self.controller.stat_table[stat_header_zone] == self.metrics_comb[metric_header_zone][i]) &
                                              (self.controller.stat_table[stat_header_type] == self.metrics_comb[metric_header_type][i])]

            self.metrics_comb.loc[i, "Combination Count"] = len(comb_frame)
        return True

    def prepare_tower_metrics(self, tower_type: str = "Tower Type"):
        if self.controller.stat_table is None:
            raise Exception("Import table before analyze")

        if self.metrics_tower is None:
            self.metrics_tower = pandas.DataFrame()

        self.fill_tower(tower_type)
        self.calc_tower_count(tower_type, "Tower Type")

    def prepare_zone_metrics(self, zone: str = "Attack Zone"):
        if self.controller.stat_table is None:
            raise Exception("Import table before analyze")

        if self.metrics_zone is None:
            self.metrics_zone = pandas.DataFrame()

        self.fill_zone(zone)
        self.calc_zone_count(zone, "Attack Zone")

    def prepare_type_metrics(self, damage_type: str = "Damage Type"):
        if self.controller.stat_table is None:
            raise Exception("Import table before analyze")

        if self.metrics_type is None:
            self.metrics_type = pandas.DataFrame()

        self.fill_type(damage_type)
        self.calc_type_count(damage_type, "Damage Type")

    def prepare_comb_metrics(self, tower_type: str = "Tower Type", attack_zone: str = "Attack Zone",
                             damage_type: str = "Damage Type"):
        if self.controller.stat_table is None:
            raise Exception("Import table before analyze")

        if self.metrics_comb is None:
            self.metrics_comb = pandas.DataFrame()

        self.fill_combinations(tower_type, "Attack Zone", damage_type)
        self.calc_combinations_count(tower_type, tower_type, "Attack Zone", attack_zone, damage_type, damage_type)

    def avr_tower_damage(self, tower_type: str = "Tower Type", full_damage: str = "Full Damage"):
        self.prepare_tower_metrics(tower_type)

        for i in range(len(self.metrics_tower)):
            tower_frame = self.controller.stat_table.loc[self.controller.stat_table[tower_type] == self.metrics_tower["Tower Type"][i]]
            type_damage = tower_frame[full_damage].sum()
            self.metrics_tower.loc[i, "Average Tower Type Damage"] = (
                        type_damage / self.metrics_tower["Tower Count"][i])

        return self.metrics_tower

    def avr_zone_damage(self, zone: str = "Attack Zone", full_damage: str = "Full Damage"):
        self.prepare_zone_metrics(zone)

        for i in range(len(self.metrics_zone)):
            zone_frame = self.controller.stat_table.loc[self.controller.stat_table[zone] == self.metrics_zone["Attack Zone"][i]]
            type_damage = zone_frame[full_damage].sum()
            self.metrics_zone.loc[i, "Average Zone Damage"] = (type_damage/self.metrics_zone["Zone Count"][i])

        return self.metrics_zone

    def avr_type_damage(self, damage_type: str = "Damage Type", full_damage: str = "Full Damage"):
        self.prepare_type_metrics(damage_type)

        for i in range(len(self.metrics_type)):
            type_frame = self.controller.stat_table.loc[self.controller.stat_table[damage_type] == self.metrics_type["Damage Type"][i]]
            type_damage = type_frame[full_damage].sum()
            self.metrics_type.loc[i, "Average Type Damage"] = (type_damage/self.metrics_type["Damage Type Count"][i])

        return self.metrics_type

    def avr_comb_damage(self, tower_type: str = "Tower Type", attack_zone: str = "Attack Zone",
                        damage_type: str = "Damage Type", full_damage: str = "Full Damage"):
        self.prepare_comb_metrics(tower_type, attack_zone, damage_type)

        for i in range(len(self.metrics_comb)):
            comb_frame = self.controller.stat_table.loc[(self.controller.stat_table[tower_type] == self.metrics_comb[tower_type][i]) &
                                              (self.controller.stat_table["Attack Zone"] == self.metrics_comb[attack_zone][i]) &
                                              (self.controller.stat_table[damage_type] == self.metrics_comb[damage_type][i])]
            comb_damage = comb_frame[full_damage].sum()
            self.metrics_comb.loc[i, "Average Combination Damage"] = (comb_damage/self.metrics_comb["Combination Count"][i])

        return self.metrics_comb

    def avr_tower_damage_cost(self, tower_type: str = "Tower Type", full_damage: str = "Full Damage", cost: str = "Cost"):
        self.avr_tower_damage(tower_type, full_damage)
        tower_costs = []

        for tower in self.metrics_tower[tower_type].tolist():
            cost_row = self.controller.char_table.loc[self.controller.char_table[tower_type] == tower]
            tower_costs.append(
                (self.metrics_tower.loc[self.metrics_tower[tower_type] == tower].iloc[0]["Average Tower Type Damage"]) / (
                cost_row.iloc[0][cost]))

        self.metrics_tower.insert(len(self.metrics_tower.columns), "Tower Type Damage Cost", tower_costs, True)
        return self.metrics_tower

    def avr_zone_damage_cost(self, zone: str = "Attack Zone", full_damage: str = "Full Damage", cost: str = "Cost"):
        self.avr_zone_damage(zone, full_damage)
        zone_costs = []

        for zone in self.metrics_zone["Attack Zone"].tolist():
            print(self.controller.char_table)
            cost_row = self.controller.char_table.loc[self.controller.char_table["Attack Zone"] == zone]
            zone_costs.append(
                (self.metrics_zone.loc[self.metrics_zone["Attack Zone"] == zone].iloc[0][
                    "Average Zone Damage"]) / (
                    cost_row.iloc[0][cost]))

        self.metrics_zone.insert(len(self.metrics_zone.columns), "Attack Zone Damage Cost", zone_costs, True)
        return self.metrics_zone

    def avr_type_damage_cost(self, damage_type: str = "Damage Type", full_damage: str = "Full Damage", cost: str = "Cost"):
        self.avr_type_damage(damage_type, full_damage)
        damage_type_costs = []

        for damage in self.metrics_type[damage_type].tolist():
            cost_row = self.controller.char_table.loc[self.controller.char_table[damage_type] == damage]
            damage_type_costs.append(
                (self.metrics_type.loc[self.metrics_type[damage_type] == damage].iloc[0][
                    "Average Type Damage"]) / (
                    cost_row.iloc[0][cost]))

        self.metrics_type.insert(len(self.metrics_type.columns), "Damage Type Damage Cost", damage_type_costs, True)
        return self.metrics_type

    def avr_comb_damage_cost(self, tower_type: str = "Tower Type", attack_zone: str = "Attack Zone",
                             damage_type: str = "Damage Type", full_damage: str = "Full Damage", cost: str = "Cost"):
        self.avr_comb_damage(tower_type, attack_zone, damage_type, full_damage)
        comb_costs = []

        print(self.metrics_comb)
        for i in range(len(self.metrics_comb)):
            cost_row = self.controller.char_table.loc[(self.controller.char_table[damage_type] == self.metrics_comb.iloc[i][damage_type]) &
                                            (self.controller.char_table[attack_zone] == self.metrics_comb.iloc[i][attack_zone]) &
                                            (self.controller.char_table[tower_type] == self.metrics_comb.iloc[i][tower_type])]
            comb_costs.append(
                (self.metrics_comb.iloc[i][
                    "Average Combination Damage"]) / (
                    cost_row.iloc[0][cost]))

        self.metrics_comb.insert(len(self.metrics_comb.columns), "Combination Damage Cost", comb_costs, True)
        return self.metrics_comb