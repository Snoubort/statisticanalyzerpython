import dearpygui.dearpygui as dpg
import pandas as pd

import AnalyzeController
import PlotController


class AppUi:
    window_tag = "Core Window"
    window = None
    minion_analyzer = None
    ability_analyzer = None
    tower_analyzer = None
    controls_bar = None
    tables_bar = None
    plots_bar = None

    def __init__(self, analyzer_controller):
        dpg.create_context()

        self.add_values()
        self.create_stat_file_dialog(analyzer_controller)
        self.create_char_file_dialog(analyzer_controller)

        with dpg.window(width=800, height=300, tag=self.window_tag, autosize=True):
            self.add_tab_bar()
        self.add_minion_controls("MinionControlsBar", analyzer_controller)
        self.add_ability_controls("AbilityControlsBar", analyzer_controller)
        self.add_tower_controls("TowerControlsBar", analyzer_controller)

        dpg.create_viewport(title='Game Statistic Analyser', width=800, height=600)
        dpg.setup_dearpygui()
        dpg.show_viewport()
        dpg.set_primary_window(self.window_tag, True)
        dpg.start_dearpygui()
        dpg.destroy_context()

    def add_tab_bar(self):
        bar = dpg.add_tab_bar(parent=self.window_tag)
        self.controls_bar = dpg.add_tab(label="Controls", tag="ControlsBar", parent=bar)
        self.tables_bar = dpg.add_tab(label="Tables", tag="TablesBar", parent=bar)
        self.plots_bar = dpg.add_tab(label="Plots", tag="PlotsBar", parent=bar)

        bar = dpg.add_tab_bar(parent=self.controls_bar)
        dpg.add_tab(label="Minion Controls", tag="MinionControlsBar", parent=bar)
        dpg.add_tab(label="Ability Controls", tag="AbilityControlsBar", parent=bar)
        dpg.add_tab(label="Tower Controls", tag="TowerControlsBar", parent=bar)

    def add_values(self):
        with dpg.value_registry():
            dpg.add_bool_value(tag="IsDeathPointsPlot")

            #Minion Metrics
            dpg.add_bool_value(tag="IsAverageLifeTime")
            dpg.add_bool_value(tag="IsAverageLifeTimePlot")
            dpg.add_bool_value(tag="IsAverageDistance")
            dpg.add_bool_value(tag="IsAverageDistancePlot")

            #Minion Round Metrics
            dpg.add_bool_value(tag="IsAverageLifeTimeRound")
            dpg.add_bool_value(tag="IsAverageLifeTimeRoundPlot")
            dpg.add_bool_value(tag="IsAverageDistanceRound")
            dpg.add_bool_value(tag="IsAverageDistancePlotRound")

            #Minion Cost Metrics
            dpg.add_bool_value(tag="IsAverageLifeTimeCost")
            dpg.add_bool_value(tag="IsAverageLifeTimeCostPlot")
            dpg.add_bool_value(tag="IsAverageDistanceCost")
            dpg.add_bool_value(tag="IsAverageDistanceCostPlot")

            # Minion Round Cost Metrics
            dpg.add_bool_value(tag="IsAverageLifeTimeRoundCost")
            dpg.add_bool_value(tag="IsAverageLifeTimeRoundCostPlot")
            dpg.add_bool_value(tag="IsAverageDistanceRoundCost")
            dpg.add_bool_value(tag="IsAverageDistanceCostRoundPlot")

            # Average Time Ability
            dpg.add_bool_value(tag="IsAverageTimeAbilities")
            dpg.add_bool_value(tag="IsAverageTimeAbilitiesPlot")

            # Casts To Cost
            dpg.add_bool_value(tag="IsCastsToCost")
            dpg.add_bool_value(tag="IsCastsToCostPlot")

            # Tower Metrics
            dpg.add_bool_value(tag="IsAverageTowerDamage")
            dpg.add_bool_value(tag="IsAverageTowerDamagePlot")
            dpg.add_bool_value(tag="IsAverageTowerDamageCost")
            dpg.add_bool_value(tag="IsAverageTowerDamageCostPlot")

            # Tower Zone Metrics
            dpg.add_bool_value(tag="IsAverageZoneDamage")
            dpg.add_bool_value(tag="IsAverageZoneDamagePlot")
            dpg.add_bool_value(tag="IsAverageZoneDamageCost")
            dpg.add_bool_value(tag="IsAverageZoneDamageCostPlot")

            # Tower Type Metrics
            dpg.add_bool_value(tag="IsAverageTypeDamage")
            dpg.add_bool_value(tag="IsAverageTypeDamagePlot")
            dpg.add_bool_value(tag="IsAverageTypeDamageCost")
            dpg.add_bool_value(tag="IsAverageTypeDamageCostPlot")

            # Tower Combination Metrics
            dpg.add_bool_value(tag="IsAverageCombinationDamage")
            dpg.add_bool_value(tag="IsAverageCombinationDamagePlot")
            dpg.add_bool_value(tag="IsAverageCombinationDamageCost")
            dpg.add_bool_value(tag="IsAverageCombinationDamageCostPlot")

    def create_stat_file_dialog(self, analyzer_controller):
        dialog = dpg.add_file_dialog(show=False, callback=lambda sender, data:
                            UiCallbacks.import_stat_data(data, analyzer_controller),
                             tag="StatFileDialog", width=700, height=400)
        dpg.add_file_extension(".csv", color=(0, 255, 0, 255), custom_text="[CsvTable]", parent=dialog)

    def create_char_file_dialog(self, analyzer_controller):
        with dpg.file_dialog(show=False, callback=lambda sender, data:
                            UiCallbacks.import_char_data(data, analyzer_controller),
                             id="CharFileDialog", width=700, height=400):
            dpg.add_file_extension(".csv", color=(0, 255, 0, 255), custom_text="[CsvTable]")

    def add_alt_checkboxes(self, parent_tag):
        # Window
        alt_block = dpg.add_child_window(label="AverageLifeTime", autosize_x=True, height=130,
                                         menubar=True, parent=parent_tag)
        menu_bar = dpg.add_menu_bar(parent=alt_block)
        dpg.add_text(default_value="Average Life Time", parent=menu_bar)

        # Full Game
        dpg.add_group(tag="ALTFullGame", horizontal=True, parent=alt_block)
        dpg.add_checkbox(label="Full Game Metric", source="IsAverageLifeTime", parent="ALTFullGame")
        dpg.add_checkbox(label="Full Game Plot", source="IsAverageLifeTimePlot", parent="ALTFullGame")

        # Rounds
        dpg.add_group(tag="ALTRounds", horizontal=True, parent=alt_block)
        dpg.add_checkbox(label="Rounds Metric", source="IsAverageLifeTimeRound", parent="ALTRounds")
        dpg.add_checkbox(label="Rounds Plot", source="IsAverageLifeTimeRoundPlot", parent="ALTRounds")

        # Cost
        dpg.add_group(tag="ALTCost", horizontal=True, parent=alt_block)
        dpg.add_checkbox(label="Cost Metric", source="IsAverageLifeTimeCost", parent="ALTCost")
        dpg.add_checkbox(label="Cost Plot", source="IsAverageLifeTimeCostPlot", parent="ALTCost")

        # Round Cost
        dpg.add_group(tag="ALTRoundCost", horizontal=True, parent=alt_block)
        dpg.add_checkbox(label="Cost Round Metric", source="IsAverageLifeTimeRoundCost", parent="ALTRoundCost")
        dpg.add_checkbox(label="Cost Round Plot", source="IsAverageLifeTimeRoundCostPlot", parent="ALTRoundCost")

    def add_average_distance_checkboxes(self, parent_tag):
        # Window
        alt_block = dpg.add_child_window(label="AverageLifeTime", autosize_x=True, height=130,
                                         menubar=True, parent=parent_tag)
        menu_bar = dpg.add_menu_bar(parent=alt_block)
        dpg.add_text(default_value="Average Distance", parent=menu_bar)

        # Full Game
        dpg.add_group(tag="ADFullGame", horizontal=True, parent=alt_block)
        dpg.add_checkbox(label="Full Game Metric", source="IsAverageDistance", parent="ADFullGame")
        dpg.add_checkbox(label="Full Game Plot", source="IsAverageDistancePlot", parent="ADFullGame")

        # Rounds
        dpg.add_group(tag="ADRounds", horizontal=True, parent=alt_block)
        dpg.add_checkbox(label="Rounds Metric", source="IsAverageDistanceRound", parent="ADRounds")
        dpg.add_checkbox(label="Rounds Plot", source="IsAverageDistanceRoundPlot", parent="ADRounds")

        # Cost
        dpg.add_group(tag="ADCost", horizontal=True, parent=alt_block)
        dpg.add_checkbox(label="Cost Metric", source="IsAverageDistanceCost", parent="ADCost")
        dpg.add_checkbox(label="Cost Plot", source="IsAverageDistanceCostPlot", parent="ADCost")

        # Round Cost
        dpg.add_group(tag="ADRoundCost", horizontal=True, parent=alt_block)
        dpg.add_checkbox(label="Cost Round Metric", source="IsAverageDistanceRoundCost", parent="ADRoundCost")
        dpg.add_checkbox(label="Cost Round Plot", source="IsAverageDistanceCostRoundPlot", parent="ADRoundCost")

    def add_average_time_abilities_checkboxes(self, parent_tag):
        # Window
        alt_block = dpg.add_child_window(autosize_x=True, height=80,
                                         menubar=True, parent=parent_tag)
        menu_bar = dpg.add_menu_bar(parent=alt_block)
        dpg.add_text(default_value="Average Time Between Abilities", parent=menu_bar)

        dpg.add_checkbox(label="Metric", source="IsAverageTimeAbilities", parent=alt_block)
        dpg.add_checkbox(label="Plot", source="IsAverageTimeAbilitiesPlot", parent=alt_block)

    def add_casts_to_cost_checkboxes(self, parent_tag):
        # Window
        alt_block = dpg.add_child_window(autosize_x=True, height=80,
                                         menubar=True, parent=parent_tag)
        menu_bar = dpg.add_menu_bar(parent=alt_block)
        dpg.add_text(default_value="Casts To Costs", parent=menu_bar)

        dpg.add_checkbox(label="Metric", source="IsCastsToCost", parent=alt_block)
        dpg.add_checkbox(label="Plot", source="IsCastsToCostPlot", parent=alt_block)

    def add_average_damage_checkboxes(self, parent_tag):
        # Window
        alt_block = dpg.add_child_window(autosize_x=True, height=220,
                                         menubar=True, parent=parent_tag)
        menu_bar = dpg.add_menu_bar(parent=alt_block)
        dpg.add_text(default_value="Average Damage", parent=menu_bar)

        group = dpg.add_group(horizontal=True, parent=alt_block)
        dpg.add_checkbox(label="Tower Metric", source="IsAverageTowerDamage", parent=group)
        dpg.add_checkbox(label="Tower Plot", source="IsAverageTowerDamagePlot", parent=group)

        group = dpg.add_group(horizontal=True, parent=alt_block)
        dpg.add_checkbox(label="Tower Cost Metric", source="IsAverageTowerDamageCost", parent=group)
        dpg.add_checkbox(label="Tower Cost Plot", source="IsAverageTowerDamageCostPlot", parent=group)

        group = dpg.add_group(horizontal=True, parent=alt_block)
        dpg.add_checkbox(label="Type Metric", source="IsAverageTypeDamage", parent=group)
        dpg.add_checkbox(label="Type Plot", source="IsAverageTypeDamagePlot", parent=group)

        group = dpg.add_group(horizontal=True, parent=alt_block)
        dpg.add_checkbox(label="Type Cost Metric", source="IsAverageTypeDamageCost", parent=group)
        dpg.add_checkbox(label="Type Cost Plot", source="IsAverageTypeDamageCostPlot", parent=group)

        group = dpg.add_group(horizontal=True, parent=alt_block)
        dpg.add_checkbox(label="Zone Metric", source="IsAverageZoneDamage", parent=group)
        dpg.add_checkbox(label="Zone Plot", source="IsAverageZoneDamagePlot", parent=group)

        group = dpg.add_group(horizontal=True, parent=alt_block)
        dpg.add_checkbox(label="Zone Cost Metric", source="IsAverageZoneDamageCost", parent=group)
        dpg.add_checkbox(label="Zone Cost Plot", source="IsAverageZoneDamageCostPlot", parent=group)

        group = dpg.add_group(horizontal=True, parent=alt_block)
        dpg.add_checkbox(label="Combination Metric", source="IsAverageCombinationDamage", parent=group)
        dpg.add_checkbox(label="Combination Plot", source="IsAverageCombinationDamagePlot", parent=group)

        group = dpg.add_group(horizontal=True, parent=alt_block)
        dpg.add_checkbox(label="Combination Cost Metric", source="IsAverageCombinationDamageCost", parent=group)
        dpg.add_checkbox(label="Combination Cost Plot", source="IsAverageCombinationDamageCostPlot", parent=group)

    def add_minion_metrics_checkboxes(self, parent_tag):
        self.add_alt_checkboxes(parent_tag)
        self.add_average_distance_checkboxes(parent_tag)

    def add_ability_metrics_checkboxes(self, parent_tag):
        self.add_average_time_abilities_checkboxes(parent_tag)
        self.add_casts_to_cost_checkboxes(parent_tag)

    def add_tower_metrics_checkboxes(self, parent_tag):
        self.add_average_damage_checkboxes(parent_tag)

    def add_minion_controls(self, parent_tag, analyzer_controller):
        dpg.add_button(label="Import Minion Csv", tag="MinionCSV",
                       callback=lambda: dpg.show_item("StatFileDialog"), parent=parent_tag)
        dpg.add_button(label="Import Minion Cost", tag="MinionCost", parent=parent_tag,
                       callback=lambda: dpg.show_item("CharFileDialog"))
        self.add_minion_metrics_checkboxes(parent_tag)
        dpg.add_button(label="Gen Minion Metrics", tag="GenMinionMetric", parent=parent_tag,
                       callback=lambda: UiCallbacks.gen_metrics(analyzer_controller, 0))
        dpg.add_button(label="Gen Minion Metrics Graphs", tag="GenMinionMetricGraphs", parent=parent_tag,
                       callback=lambda: UiCallbacks.gen_metric_plots(analyzer_controller, self.plots_bar))

    def add_ability_controls(self, parent_tag, analyzer_controller):
        dpg.add_button(label="Import Ability Csv", tag="AbilityCSV",
                       callback=lambda: dpg.show_item("StatFileDialog"), parent=parent_tag)
        dpg.add_button(label="Import Ability Cost", tag="AbilityCost", parent=parent_tag,
                       callback=lambda: dpg.show_item("CharFileDialog"))
        self.add_ability_metrics_checkboxes(parent_tag)
        dpg.add_button(label="Gen Ability Metrics", tag="GenAbilityMetric", parent=parent_tag,
                       callback=lambda: UiCallbacks.gen_metrics(analyzer_controller, 1))
        dpg.add_button(label="Gen Ability Metrics Graphs", tag="GenAbilityMetricGraphs", parent=parent_tag,
                       callback=lambda: UiCallbacks.gen_metric_plots(analyzer_controller, self.plots_bar))

    def add_tower_controls(self, parent_tag, analyzer_controller):
        dpg.add_button(label="Import Tower Csv", tag="TowerCSV",
                       callback=lambda: dpg.show_item("StatFileDialog"), parent=parent_tag)
        dpg.add_button(label="Import Tower Cost", tag="TowerCost", parent=parent_tag,
                       callback=lambda: dpg.show_item("CharFileDialog"))
        self.add_tower_metrics_checkboxes(parent_tag)
        dpg.add_button(label="Gen Tower Metrics", tag="GenTowerMetric", parent=parent_tag,
                       callback=lambda: UiCallbacks.gen_metrics(analyzer_controller, 2))
        dpg.add_button(label="Gen Tower Metrics Graphs", tag="GenTowerMetricGraphs", parent=parent_tag,
                       callback=lambda: UiCallbacks.gen_metric_plots(analyzer_controller, self.plots_bar))

class UiCallbacks:
    @staticmethod
    def selections_to_paths(selections):
        tables_paths = []
        for key in selections.keys():
            tables_paths.append(selections[key])
        return tables_paths

    @staticmethod
    def import_stat_data(file_dialog_data, controller: AnalyzeController.AnalyzeController):
        controller.csv_paths = UiCallbacks.selections_to_paths(file_dialog_data["selections"])
        controller.import_from_csv()

    @staticmethod
    def import_char_data(file_dialog_data, controller: AnalyzeController.AnalyzeController):
        controller.characteristic_paths = UiCallbacks.selections_to_paths(file_dialog_data["selections"])
        controller.import_characteristics()

    @staticmethod
    def set_tab_activ(set_window, tab_bar):
        dpg.hide_item(set_window)

    @staticmethod
    def gen_metrics(controller: AnalyzeController, statistic_type: int):

        if statistic_type == 0:
            controller.minion_analyzer.prepare_minion_metrics()
            controller.minion_analyzer.prepare_minion_round_metrics()
        elif statistic_type == 1:
            controller.ability_analyzer.prepare_ability_metrics()
        elif statistic_type == 2:
            controller.tower_analyzer.prepare_tower_metrics()
            controller.tower_analyzer.prepare_type_metrics()
            controller.tower_analyzer.prepare_zone_metrics()
            controller.tower_analyzer.prepare_comb_metrics()

        controller.gen_metrics_for_values()

        if statistic_type == 0:
            controller.gen_minion_table("TablesBar")
        elif statistic_type == 1:
            controller.gen_ability_table("TablesBar")
        elif statistic_type == 2:
            controller.gen_tower_table("TablesBar")


    @staticmethod
    def gen_metric_plots(controller: AnalyzeController, plot_window):
        PlotController.gen_plots_for_values(controller, plot_window)