import pandas
import dearpygui.dearpygui as dpg

from MinionAnalyzer import MinionAnalyzer
from AbilityAnalyzer import AbilityAnalyzer
from TowerAnalyzer import TowerAnalyzer


class AnalyzeController:
    csv_paths: list[str] = []
    characteristic_paths: list[str] = []

    stat_table: pandas.DataFrame = None
    char_table: pandas.DataFrame = None

    minion_analyzer = MinionAnalyzer()
    ability_analyzer = AbilityAnalyzer()
    tower_analyzer = TowerAnalyzer()

    __is_minion_table = False
    __is_minion_round_table = False

    __is_tower_table = False
    __is_type_table = False
    __is_zone_table = False
    __is_comb_table = False

    def __init__(self):
        self.minion_analyzer.controller = self
        self.ability_analyzer.controller = self
        self.tower_analyzer.controller = self

    def import_from_csv(self) -> pandas.DataFrame:
        if len(self.csv_paths) == 0:
            raise Exception("There is no csv_paths")

        self.stat_table = pandas.DataFrame()
        for table in self.csv_paths:
            self.stat_table = self.stat_table._append(pandas.read_csv(table, sep=";"), ignore_index=True)

        return self.stat_table

    def import_characteristics(self) -> pandas.DataFrame:
        if len(self.characteristic_paths) == 0:
            raise Exception("There is no char_paths")

        self.char_table = pandas.DataFrame()
        for table in self.characteristic_paths:
            self.char_table = self.char_table._append(pandas.read_csv(table, sep=";"), ignore_index=True)

        return self.char_table

    def gen_metrics_for_values(self):
        if dpg.get_value("IsAverageLifeTime"):
            self.minion_analyzer.average_life_time()
            self.__is_minion_table = True
        if dpg.get_value("IsAverageDistance"):
            self.minion_analyzer.average_distance()
            self.__is_minion_table = True
        if dpg.get_value("IsAverageLifeTimeRound"):
            self.minion_analyzer.average_life_round_time()
            self.__is_minion_round_table = True
        if dpg.get_value("IsAverageDistanceRound"):
            self.minion_analyzer.average_round_distance()
            self.__is_minion_round_table = True
        if dpg.get_value("IsAverageLifeTimeCost"):
            self.minion_analyzer.average_life_time_cost()
            self.__is_minion_table = True
        if dpg.get_value("IsAverageDistanceCost"):
            self.minion_analyzer.average_distance_cost()
            self.__is_minion_table = True
        if dpg.get_value("IsAverageLifeTimeRoundCost"):
            self.minion_analyzer.average_life_round_time_cost()
            self.__is_minion_round_table = True
        if dpg.get_value("IsAverageDistanceRoundCost"):
            self.minion_analyzer.average_round_distance_cost()
            self.__is_minion_round_table = True

        if dpg.get_value("IsAverageTimeAbilities"):
            self.ability_analyzer.average_time_abilities()
        if dpg.get_value("IsCastsToCost"):
            self.ability_analyzer.casts_to_cost()

        if dpg.get_value("IsAverageTowerDamage"):
            self.tower_analyzer.avr_tower_damage()
            self.__is_tower_table = True
        if dpg.get_value("IsAverageTowerDamageCost"):
            self.tower_analyzer.avr_tower_damage_cost()
            self.__is_tower_table = True
        if dpg.get_value("IsAverageZoneDamage"):
            self.tower_analyzer.avr_zone_damage()
            self.__is_zone_table = True
        if dpg.get_value("IsAverageZoneDamageCost"):
            self.tower_analyzer.avr_zone_damage_cost()
            self.__is_zone_table = True
        if dpg.get_value("IsAverageTypeDamage"):
            self.tower_analyzer.avr_type_damage()
            self.__is_type_table = True
        if dpg.get_value("IsAverageTypeDamageCost"):
            self.tower_analyzer.avr_type_damage_cost()
            self.__is_type_table = True
        if dpg.get_value("IsAverageCombinationDamage"):
            self.tower_analyzer.avr_comb_damage()
            self.__is_comb_table = True
        if dpg.get_value("IsAverageCombinationDamageCost"):
            self.tower_analyzer.avr_comb_damage_cost()
            self.__is_comb_table = True

    def gen_minion_table(self, window):

        # Gen metric for all game metrics
        if self.__is_minion_table:
            metric_headers = self.minion_analyzer.minion_metrics.columns

            dpg.add_text(parent=window, default_value="All Game Metrics")
            table = dpg.add_table(tag="MinionTable", parent=window, borders_innerH=True, borders_innerV=True,
                                  borders_outerH=True, borders_outerV=True)

            for header in metric_headers:
                dpg.add_table_column(label=header, parent=table)
            for i in range(0, len(self.minion_analyzer.minion_metrics.index)):
                current_row = dpg.add_table_row(parent=table)
                for header in metric_headers:
                    dpg.add_text(self.minion_analyzer.minion_metrics.at[i, header], parent=current_row)

        # Gen table for round metrics
        if self.__is_minion_round_table:
            metric_headers = self.minion_analyzer.minion_round_metrics.columns

            dpg.add_text(parent=window, default_value="Round Metrics")
            table = dpg.add_table(tag="MinionRoundTable", parent=window, borders_innerH=True, borders_innerV=True,
                                  borders_outerH=True, borders_outerV=True)

            for header in metric_headers:
                dpg.add_table_column(label=header, parent=table)

            for i in range(0, len(self.minion_analyzer.minion_round_metrics.index)):
                current_row = dpg.add_table_row(parent=table)
                for header in metric_headers:
                    dpg.add_text(self.minion_analyzer.minion_round_metrics.at[i, header], parent=current_row)

    def gen_ability_table(self, window):
        metric_headers = self.ability_analyzer.metrics.columns

        dpg.add_text(parent=window, default_value="All Game Metrics")
        table = dpg.add_table(tag="AbilityTable", parent=window, borders_innerH=True, borders_innerV=True,
                              borders_outerH=True, borders_outerV=True)

        for header in metric_headers:
            dpg.add_table_column(label=header, parent=table)
        for i in range(0, len(self.ability_analyzer.metrics.index)):
            current_row = dpg.add_table_row(parent=table)
            for header in metric_headers:
                dpg.add_text(self.ability_analyzer.metrics.at[i, header], parent=current_row)

    def gen_tower_table(self, window):
        print("Yes Yes Yes Yes")
        print(self.__is_tower_table)
        if self.__is_tower_table:
            metric_headers = self.tower_analyzer.metrics_tower.columns

            dpg.add_text(parent=window, default_value="All Game Metrics")
            table = dpg.add_table(tag="TowerTable", parent=window, borders_innerH=True, borders_innerV=True,
                                  borders_outerH=True, borders_outerV=True)

            for header in metric_headers:
                dpg.add_table_column(label=header, parent=table)
            for i in range(0, len(self.tower_analyzer.metrics_tower.index)):
                current_row = dpg.add_table_row(parent=table)
                for header in metric_headers:
                    dpg.add_text(self.tower_analyzer.metrics_tower.at[i, header], parent=current_row)
        if self.__is_type_table:
            metric_headers = self.tower_analyzer.metrics_type.columns

            dpg.add_text(parent=window, default_value="All Game Metrics")
            table = dpg.add_table(tag="TypeTable", parent=window, borders_innerH=True, borders_innerV=True,
                                  borders_outerH=True, borders_outerV=True)

            for header in metric_headers:
                dpg.add_table_column(label=header, parent=table)
            for i in range(0, len(self.tower_analyzer.metrics_type.index)):
                current_row = dpg.add_table_row(parent=table)
                for header in metric_headers:
                    dpg.add_text(self.tower_analyzer.metrics_type.at[i, header], parent=current_row)
        if self.__is_zone_table:
            metric_headers = self.tower_analyzer.metrics_zone.columns

            dpg.add_text(parent=window, default_value="All Game Metrics")
            table = dpg.add_table(tag="ZoneTable", parent=window, borders_innerH=True, borders_innerV=True,
                                  borders_outerH=True, borders_outerV=True)

            for header in metric_headers:
                dpg.add_table_column(label=header, parent=table)
            for i in range(0, len(self.tower_analyzer.metrics_zone.index)):
                current_row = dpg.add_table_row(parent=table)
                for header in metric_headers:
                    dpg.add_text(self.tower_analyzer.metrics_zone.at[i, header], parent=current_row)
        if self.__is_comb_table:
            metric_headers = self.tower_analyzer.metrics_comb.columns

            dpg.add_text(parent=window, default_value="All Game Metrics")
            table = dpg.add_table(tag="CombTable", parent=window, borders_innerH=True, borders_innerV=True,
                                  borders_outerH=True, borders_outerV=True)

            for header in metric_headers:
                dpg.add_table_column(label=header, parent=table)
            for i in range(0, len(self.tower_analyzer.metrics_comb.index)):
                current_row = dpg.add_table_row(parent=table)
                for header in metric_headers:
                    dpg.add_text(self.tower_analyzer.metrics_comb.at[i, header], parent=current_row)