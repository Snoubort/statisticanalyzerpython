import dearpygui.dearpygui as dpg
import pandas


def selections_to_paths(selections):
    tables_paths = []
    for key in selections.keys():
        tables_paths.append(selections[key])
    return tables_paths


def callback(sender, app_data, user_data):
    user_data.csv_paths = selections_to_paths(app_data["selections"])
    user_data.import_from_csv()

    user_data.char_path = ["E:\\StatData\\CostSpellTest.csv"]
    user_data.import_characteristics()


    """user_data.fill_combinations("Tower Type", "Damage Zone", "Damage Type")
    user_data.calc_combinations_count("Tower Type", "Tower Type",
    "Damage Zone", "Attack Zone",
    "Damage Type", "Damage Type")"""

    # user_data.fill_tower("Tower Type")
    # user_data.calc_tower_count("Tower Type", "Tower Type")
    # user_data.avr_type_damage("Tower Type", "Full Damage")

    user_data.casts_to_cost()

    # user_data.avr_comb_damage_cost("Tower Type", "Attack Zone", "Damage Type", "Full Damage", "Cost")
    # user_data.avr_type_damage_cost("Damage Type", "Full Damage", "Cost")
    #user_data.avr_zone_damage_cost("Damage Zone", "Full Damage", "Cost")
    #user_data.avr_tower_damage_cost("Tower Type", "Full Damage", "Cost")
    #user_data.avr_comb_damage("Tower Type", "Attack Zone", "Damage Type", "Full Damage")
    # user_data.avr_type_damage("Damage Type", "Full Damage")
    # user_data.avr_zone_damage("Damage Zone", "Full Damage")

    # user_data.average_distance_cost("Monster Type", "Spawn Point", "Death Point", "Cost")
    # user_data.average_life_time_cost("Monster Type", "Spawn Time", "Death Time", "Cost")

    # user_data.average_life_round_time_cost("Monster Type", "Round Number", "Spawn Time", "Death Time", "Cost")
    # user_data.average_round_distance_cost("Monster Type", "Round Number", "Spawn Point", "Death Point", "Cost")

    with pandas.option_context("display.max_rows", None, "display.max_columns", None):
        print(user_data.metrics)
        print(user_data.stat_table)
        # print(user_data.minion_metrics)
        # print(user_data.minion_round_metrics)

def add_plot(sender, app_data):
    plot_parent = dpg.get_item_parent(sender)
    plot = dpg.add_plot(label="Test Plot", parent=plot_parent)

def file_dialog(user_data):
    with dpg.file_dialog(show=False, callback=callback, user_data=user_data, id="file_dialog_id", width=700,
                         height=400):
        dpg.add_file_extension(".csv", color=(0, 255, 0, 255), custom_text="[CsvTable]")


def file_dialog_button():
    with dpg.window(label="Analyser", width=800, height=300, tag="CoreWindow"):
        dpg.add_button(label="CsvSelector", callback=lambda: dpg.show_item("file_dialog_id"))
        dpg.add_button(label="AddPlot", callback=add_plot)
        dpg.add_checkbox(label="Average Life Time", source="IsAverageLifeTime")
        dpg.add_checkbox(label="Average Distance", source="IsAverageDistance")
        dpg.add_checkbox(label="Round Metrics", source="IsMinionRoundMetrics")
        dpg.add_checkbox(label="Cost Metrics", source="IsUseCost")

