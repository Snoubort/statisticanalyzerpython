import math
import pandas


class MinionAnalyzer:
    controller = None

    minion_metrics: pandas.DataFrame = None
    minion_round_metrics: pandas.DataFrame = None

    in_minion_type: str = "Minion Type"
    in_spawn_time: str = "Spawn Time"
    in_death_time: str = "Death Time"

    metric_minion_type: str = "Minion Type"
    minion_count: str = "Minion Count"
    average_life_time: str = "Average Life Time"

    def fill_minion_type(self) -> bool:
        if self.metric_minion_type in self.minion_metrics.columns:
            return False

        self.minion_metrics[self.metric_minion_type] = self.controller.stat_table.drop_duplicates(subset=[self.in_minion_type]).loc[:, self.in_minion_type]

        self.minion_metrics = self.minion_metrics.reset_index(drop=True)
        return True

    def calc_minion_count(self) -> bool:
        if self.minion_count in self.minion_metrics.columns:
            return False

        minion_counts = []
        for minion in self.minion_metrics.loc[: ,self.metric_minion_type].tolist():
            minion_frame = self.controller.stat_table.loc[self.controller.stat_table[self.in_minion_type] == minion]
            minion_counts.append(len(minion_frame))

        self.minion_metrics.insert(len(self.minion_metrics.columns), self.minion_count, minion_counts, True)
        return True

    def prepare_minion_metrics(self):
        if self.controller.stat_table is None:
            raise Exception("Import table before analyze")

        if self.minion_metrics is None:
            self.minion_metrics = pandas.DataFrame()

        self.fill_minion_type()
        self.calc_minion_count()

    def average_life_time(self, minion_type: str = "Minion Type", minion_count: str = "Minion Count",
                          spawn_time: str = "Spawn Time", death_time: str = "Death Time",
                          average_life_time: str = "Average Life Time") -> pandas.DataFrame:
        self.prepare_minion_metrics()

        for i in range(len(self.minion_metrics)):
            minion = self.minion_metrics[self.metric_minion_type][i]
            minion_frame = self.controller.stat_table.loc[self.controller.stat_table[self.in_minion_type] == minion]
            type_life_time = minion_frame[death_time].sum() - minion_frame[spawn_time].sum()
            self.minion_metrics.loc[i, average_life_time] = (type_life_time/self.minion_metrics[minion_count][i])

        return self.minion_metrics.loc[:, [self.metric_minion_type, minion_count, average_life_time]]

    def point_point_distance(self, spawn_point: str = "Spawn Point", death_point: str = "Death Point") -> float:
        x_sum = pow((int(death_point[0]) - int(spawn_point[0])), 2)
        y_sum = pow((int(death_point[1]) - int(spawn_point[1])), 2)
        z_sum = pow((int(death_point[2]) - int(spawn_point[2])), 2)
        distance = math.sqrt(x_sum + y_sum + z_sum)
        return distance

    def row_dist_calc(self, row, spawn_point: str = "Spawn Point", death_point: str = "Death Point") -> float:
        return self.point_point_distance(row[spawn_point].split(" "), row[death_point].split(" "))

    def average_distance(self, minion_type: str = "Minion Type", spawn_point: str = "Spawn Point",
                         death_point: str = "Death Point", distance: str = "Distance",
                         average_distance: str = "Average Distance",
                         minion_count: str = "Minion Count") -> pandas.DataFrame:
        self.prepare_minion_metrics()

        self.controller.stat_table[distance] = self.controller.stat_table.apply(
            lambda row: self.row_dist_calc(row, spawn_point, death_point), axis=1)

        for i in range(len(self.minion_metrics)):
            minion = self.minion_metrics[self.metric_minion_type][i]
            minion_frame = self.controller.stat_table.loc[self.controller.stat_table[self.in_minion_type] == minion]
            self.minion_metrics.loc[i, average_distance] = (
                        minion_frame[distance].sum() / self.minion_metrics[minion_count][i])
        return self.minion_metrics

    def fill_round_minion_type(self, minion_type: str = "Minion Type", round_number: str = "Round Number"):
        if (self.metric_minion_type in self.minion_round_metrics.columns and
                round_number in self.minion_round_metrics.columns):
            return False

        minion_round_stat = self.controller.stat_table.drop_duplicates(subset=[self.in_minion_type,
                                                                    round_number]).loc[:, [self.in_minion_type,
                                                                                           round_number]]

        self.minion_round_metrics[round_number] = minion_round_stat.loc[:, [round_number]]
        self.minion_round_metrics[self.metric_minion_type] = minion_round_stat.loc[:, [self.in_minion_type]]

        self.minion_round_metrics = self.minion_round_metrics.reset_index(drop=True)
        return True

    def calc_round_minion_count(self, minion_type: str = "Minion Type", round_number: str = "Round Number",
                                minion_count: str = "Minion Count") -> bool:
        if minion_count in self.minion_round_metrics.columns:
            return False

        minion_round_count = []

        for round_id in self.minion_round_metrics.loc[:, round_number].drop_duplicates():
            minion_types_round = self.minion_round_metrics.loc[self.minion_round_metrics[round_number] == round_id]

            for minion in minion_types_round[self.metric_minion_type].tolist():
                minion_frame = self.controller.stat_table.loc[(self.controller.stat_table[round_number] == round_id) &
                                          (self.controller.stat_table[self.in_minion_type] == minion)]
                minion_round_count.append(len(minion_frame))

        self.minion_round_metrics.insert(len(self.minion_round_metrics.columns), minion_count,
                                         minion_round_count,True)
        return True

    def prepare_minion_round_metrics(self, minion_type: str = "Minion Type", round_number: str = "Round Number"):
        if self.controller.stat_table is None:
            raise Exception("Import table before analyze")

        if self.minion_round_metrics is None:
            self.minion_round_metrics = pandas.DataFrame()

        self.fill_round_minion_type()
        self.calc_round_minion_count()

    def average_life_round_time(self, minion_type: str = "Minion Type", round_number: str = "Round Number",
                                spawn_time: str = "Spawn Time", death_time: str = "Death Time",
                                minion_count: str = "Minion Count",
                                average_life_time: str = "Average Life Time") -> pandas.DataFrame:
        self.prepare_minion_round_metrics()

        avr_minion_life = []

        for round_id in self.minion_round_metrics.loc[:, round_number].drop_duplicates():
            minion_types_round = self.minion_round_metrics.loc[self.minion_round_metrics[round_number] == round_id]

            for minion in minion_types_round[self.metric_minion_type].tolist():
                minion_frame = self.controller.stat_table.loc[(self.controller.stat_table[round_number] == round_id) &
                                                   (self.controller.stat_table[self.in_minion_type] == minion)]
                type_life_time = minion_frame[death_time].sum() - minion_frame[spawn_time].sum()

                round_count = self.minion_round_metrics.loc[(self.minion_round_metrics[round_number] == round_id) &
                                                   (self.minion_round_metrics[self.metric_minion_type] == minion)]

                avr_minion_life.append(type_life_time/round_count.iloc[0][minion_count])

        self.minion_round_metrics.insert(len(self.minion_round_metrics.columns), average_life_time,
                                         avr_minion_life, True)

        return self.minion_round_metrics.loc[:, [self.metric_minion_type, minion_count, average_life_time]]

    def average_round_distance(self, minion_type: str = "Minion Type", round_number: str = "Round Number",
                               spawn_point: str = "Spawn Point", death_point: str = "Death Point",
                               distance: str = "Distance", minion_count: str = "Minion Count",
                               average_distance: str = "Average Distance") -> pandas.DataFrame:
        self.prepare_minion_round_metrics()

        self.controller.stat_table[distance] = self.controller.stat_table.apply(
            lambda row: self.row_dist_calc(row, spawn_point, death_point), axis=1)

        avr_round_dist = []

        for round_id in self.minion_round_metrics.loc[:, round_number].drop_duplicates():
            minion_types_round = self.minion_round_metrics.loc[self.minion_round_metrics[round_number] == round_id]

            for minion in minion_types_round[self.metric_minion_type].tolist():
                minion_frame = self.controller.stat_table.loc[(self.controller.stat_table[round_number] == round_id) &
                                                   (self.controller.stat_table[self.in_minion_type] == minion)]
                type_dists = minion_frame[distance].sum()

                round_count = self.minion_round_metrics.loc[(self.minion_round_metrics[round_number] == round_id) &
                                                            (self.minion_round_metrics[self.metric_minion_type] == minion)]

                avr_round_dist.append(type_dists / round_count.iloc[0][minion_count])

        self.minion_round_metrics.insert(len(self.minion_round_metrics.columns), average_distance,
                                         avr_round_dist, True)

        return self.minion_round_metrics.loc[:, [self.metric_minion_type, minion_count, average_distance]]

    def average_life_time_cost(self, minion_type: str = "Minion Type", minion_count: str = "Minion Count",
                               cost: str = "Cost",
                               average_life_time: str = "Average Life Time",
                               life_time_cost: str = "Life Time Cost") -> pandas.DataFrame:
        self.average_life_time()
        alt_costs = []

        for minion in self.minion_metrics[self.metric_minion_type].tolist():
            cost_row = self.controller.char_table.loc[self.controller.char_table[self.in_minion_type] == minion]
            alt_costs.append((self.minion_metrics.loc[self.minion_metrics[self.metric_minion_type] ==
                                                      minion].iloc[0][average_life_time]) / (cost_row.iloc[0][cost]))

        self.minion_metrics.insert(len(self.minion_metrics.columns), life_time_cost, alt_costs, True)
        return self.minion_metrics.loc[:, [self.metric_minion_type, minion_count, average_life_time, life_time_cost]]

    def average_distance_cost(self, minion_type: str = "Minion Type", cost: str = "Cost",
                              average_distance: str = "Average Distance", distant_cost: str = "Distant Cost") -> pandas.DataFrame:
        self.average_distance()
        distance_costs = []

        for minion in self.minion_metrics[self.metric_minion_type].tolist():
            cost_row = self.controller.char_table.loc[self.controller.char_table[self.in_minion_type] == minion]
            distance_costs.append((self.minion_metrics.loc[self.minion_metrics[self.metric_minion_type] == minion].iloc[0][average_distance]) / (cost_row.iloc[0][cost]))

        self.minion_metrics.insert(len(self.minion_metrics.columns), distant_cost, distance_costs, True)
        return self.minion_metrics

    def average_life_round_time_cost(self, cost: str = "Cost", minion_type: str = "Minion Type",
                                     average_life_time: str = "Average Life Time", round_number: str = "Round Number",
                                     average_life_time_round_cost: str = "Average Life Time Round Cost") -> pandas.DataFrame:
        self.average_life_round_time()
        life_costs = []

        for round_id in self.minion_round_metrics.loc[:, round_number].drop_duplicates():
            minion_types_round = self.minion_round_metrics.loc[self.minion_round_metrics[round_number] == round_id]

            for minion in minion_types_round[minion_type].tolist():
                cost_row = self.controller.char_table.loc[self.controller.char_table[self.in_minion_type] == minion]
                life_costs.append((self.minion_round_metrics.loc[(self.minion_round_metrics[round_number] == round_id) &
                                                   (self.minion_round_metrics[minion_type] == minion)].iloc[0][average_life_time]) / (cost_row.iloc[0][cost]))

        self.minion_round_metrics.insert(len(self.minion_round_metrics.columns),
                                         average_life_time_round_cost, life_costs, True)
        return self.minion_round_metrics

    def average_round_distance_cost(self, cost: str = "Cost", minion_type: str = "Minion Type",
                                     average_distance: str = "Average Distance", round_number: str = "Round Number",
                                    average_distance_round_cost: str = "Average Distance Round Cost") -> pandas.DataFrame:
        self.average_round_distance()
        dist_costs = []

        for round_id in self.minion_round_metrics.loc[:, round_number].drop_duplicates():
            minion_types_round = self.minion_round_metrics.loc[self.minion_round_metrics[round_number] == round_id]

            for minion in minion_types_round[minion_type].tolist():
                cost_row = self.controller.char_table.loc[self.controller.char_table[self.in_minion_type] == minion]
                dist_costs.append((self.minion_round_metrics.loc[(self.minion_round_metrics[round_number] == round_id) &
                                                   (self.minion_round_metrics[minion_type] == minion)].iloc[0][average_distance]) / (cost_row.iloc[0][cost]))

        self.minion_round_metrics.insert(len(self.minion_round_metrics.columns), average_distance_round_cost, dist_costs, True)
        print(self.minion_round_metrics)
        return self.minion_round_metrics