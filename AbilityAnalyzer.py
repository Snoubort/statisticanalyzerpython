import math
import pandas


class AbilityAnalyzer:
    controller = None

    metrics: pandas.DataFrame = None

    def fill_ability_typs(self, ability_type: str = "Ability Type") -> bool:
        if ability_type in self.metrics.columns:
            return False

        unic_combinations = self.controller.stat_table.drop_duplicates(subset=[ability_type])
        self.metrics[ability_type] = unic_combinations.loc[:, ability_type]

        self.metrics = self.metrics.reset_index(drop=True)
        return True

    def calc_ability_count(self, ability_type: str = "Ability Type", counter_header: str = "Ability Count"):
        if counter_header in self.metrics.columns:
            return False

        for i in range(len(self.metrics)):
            ability_frame = self.controller.stat_table.loc[self.controller.stat_table[ability_type] == self.metrics[ability_type][i]]
            self.metrics.loc[i, counter_header] = len(ability_frame)
        return True

    def prepare_ability_metrics(self, ability_type: str = "Ability Type"):
        if self.controller.stat_table is None:
            raise Exception("Import table before analyze")

        if self.metrics is None:
            self.metrics = pandas.DataFrame()

        self.fill_ability_typs()
        self.calc_ability_count()

    def average_time_abilities(self, ability_type: str = "Ability Type", ability_stat_time: str = "Cast Time", metric_counter: str = "Ability Count", average_time_name: str = "Average Casts Time"):
        self.prepare_ability_metrics()

        average_times = []

        for ability in self.metrics[ability_type].tolist():
            time_frame = self.controller.stat_table.loc[self.controller.stat_table[ability_type] == ability][ability_stat_time]
            max_cast_time = time_frame.max()
            min_cast_time = time_frame.min()
            type_count = self.metrics.loc[self.metrics[ability_type] == ability].iloc[0][metric_counter]

            average_times.append((max_cast_time-min_cast_time)/type_count)

        self.metrics.insert(len(self.metrics.columns), average_time_name, average_times, True)

    def casts_to_cost(self, ability_type: str = "Ability Type", count_metric: str = "Ability Count", cost_stat: str = "Cost", cast_to_cost_name: str = "Casts For Cost"):
        self.prepare_ability_metrics()
        casts_to_cost = []

        for ability in self.metrics[ability_type].tolist():
            count = self.metrics.loc[self.metrics[ability_type] == ability].iloc[0][count_metric]
            cost = self.controller.char_table.loc[self.controller.char_table[ability_type] == ability].iloc[0][cost_stat]
            casts_to_cost.append(count/cost)

        self.metrics.insert(len(self.metrics.columns), cast_to_cost_name, casts_to_cost, True)