from AnalyzeController import AnalyzeController
import dearpygui.dearpygui as dpg


def create_minion_type_axis_ticks(controller: AnalyzeController):
    print(controller.minion_analyzer.minion_metrics)
    minion_types = controller.minion_analyzer.minion_metrics["Minion Type"].tolist()

    pairs = []
    i = 1
    for minion_type in minion_types:
        pairs.append((str(minion_type), i))
        i = i+1

    print(pairs)
    return tuple(pairs)


def create_rounds_axis_ticks(controller: AnalyzeController):
    rounds = controller.minion_analyzer.minion_round_metrics["Round Number"].unique().tolist()

    pairs = []
    i = 1
    for round_number in rounds:
        pairs.append((str(round_number), i))
        i = i+1

    return tuple(pairs)


def gen_plots_for_values(controller: AnalyzeController, plot_window):
    if dpg.get_value("IsAverageLifeTimePlot"):
        generate_minion_metric_plot(controller, plot_window, "Average Life Time",
                                    "ALTAxis", "ALTPlot")
    if dpg.get_value("IsAverageDistancePlot"):
        generate_minion_metric_plot(controller, plot_window, "Average Distance",
                                    "ADAxis", "ADPlot")
    if dpg.get_value("IsAverageLifeTimeRoundPlot"):
        generate_minion_round_metrics(controller, plot_window, "Average Life Time",
                                      "ALTRoundAxis", "ALTRoundPlot")
    if dpg.get_value("IsAverageDistancePlotRound"):
        generate_minion_round_metrics(controller, plot_window, "Average Distance",
                                      "ADRoundAxis", "ADRoundPlot")
    if dpg.get_value("IsAverageLifeTimeCostPlot"):
        generate_minion_metric_plot(controller, plot_window, "Life Time Cost",
                                    "ALTCostAxis", "ALTCostPlot")
    if dpg.get_value("IsAverageDistanceCostPlot"):
        generate_minion_metric_plot(controller, plot_window, "Distant Cost",
                                    "ADCostAxis", "ADCostPlot")
    if dpg.get_value("IsAverageLifeTimeRoundCostPlot"):
        generate_minion_round_metrics(controller, plot_window, "Average Life Time Round Cost",
                                      "ALTRoundCostAxis", "ALTRoundCostPlot")
    if dpg.get_value("IsAverageDistanceCostRoundPlot"):
        generate_minion_round_metrics(controller, plot_window, "Average Distance Round Cost",
                                      "ALTRoundCostAxis", "ALTRoundCostPlot")


def generate_minion_metric_plot(controller: AnalyzeController, parent, metric_name,
                                    y_axis_name, plot_tag):
    plot = dpg.add_plot(label=metric_name, tag=plot_tag, parent=parent)
    dpg.add_plot_legend(parent=plot)

    dpg.add_plot_axis(dpg.mvXAxis, label="Minion Types", parent=plot)
    dpg.set_axis_ticks(dpg.last_item(), create_minion_type_axis_ticks(controller))

    dpg.add_plot_axis(dpg.mvYAxis, label=metric_name, tag=y_axis_name, parent=plot)

    x_values = []
    minion_types = controller.minion_analyzer.minion_metrics["Minion Type"].tolist()
    for i in range(0, len(minion_types)):
        x_values.append(i)

    y_values = controller.minion_analyzer.minion_metrics[metric_name].tolist()
    dpg.add_bar_series(x_values, y_values, parent=y_axis_name)

def generate_minion_round_metrics(controller: AnalyzeController, parent, metric_name,
                                    y_axis_name, plot_tag):
    plot = dpg.add_plot(label=metric_name, tag=plot_tag, parent=parent)
    dpg.add_plot_legend(parent=plot)

    dpg.add_plot_axis(dpg.mvXAxis, label="Rounds", parent=plot)
    dpg.set_axis_ticks(dpg.last_item(), create_rounds_axis_ticks(controller))

    dpg.add_plot_axis(dpg.mvYAxis, label=metric_name, tag=y_axis_name, parent=plot)

    x_values = []
    rounds = controller.minion_analyzer.minion_round_metrics["Round Number"].unique().tolist()
    for i in range(0, len(rounds)):
        x_values.append(i)

    y_values = []
    minion_types = controller.minion_analyzer.minion_round_metrics["Minion Type"].unique().tolist()
    for minion_type in minion_types:
        selected_type_frame = controller.minion_analyzer.minion_round_metrics.loc[
            controller.minion_analyzer.minion_round_metrics["Minion Type"] == minion_type]

        type_values = []
        for i in range(0, len(rounds)):
            metric_for_round = selected_type_frame.loc[selected_type_frame["Round Number"] == i][metric_name]
            if metric_for_round.size == 0:
                type_values.append(0)
            else:
                type_values.extend(selected_type_frame.loc[selected_type_frame["Round Number"] == i]["Round Number"].tolist())
        y_values.append(type_values)
    for type_values in y_values:
        dpg.add_line_series(x_values, type_values, parent=y_axis_name)