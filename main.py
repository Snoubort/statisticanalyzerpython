import pandas
import dearpygui.dearpygui as dpg

import WindowCreation
import AnalyzerApp
from AnalyzeController import AnalyzeController

controller:AnalyzeController = AnalyzeController()
app = AnalyzerApp.AppUi(controller)

dpg.create_viewport(title='Custom Title', width=800, height=600)

dpg.setup_dearpygui()
dpg.show_viewport()

dpg.show_documentation()

dpg.start_dearpygui()

dpg.destroy_context()

